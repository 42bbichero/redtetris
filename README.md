# Red Tetris Project

Red tetris is a school project of the 42 cursus. It's an online Tetris game with multiboard handling in multiple lobbies.


### Install

Install [node](https://nodejs.org/en/) first. After that:

```
$ npm i
```

Edit `params.js` for your needs.


### Development Mode

#### Launch Server

```
$ npm run  srv-dev
```

#### Launch Client

```
$ npm run client-dev
```

Point your browser to `http://0.0.0.0:8080/` it will load client side application. 


#### Test

```
$ npm run test
```

Tests are installed under `test` folder.


#### Coverage

```
npm run coverage
```

Check results …. of this command, and launch your browser to `./coverage/lcov-report/index.html`


### Production Mode

```
$ npm run srv-dist

$ npm run client-dist
```
