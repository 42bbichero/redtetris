// Import modules
import express from "express";
import lobbiesRoute from "./lobby/lobby.route";
import usersRoute from "./users/user.route";
// Define route
const router = express.Router();

function	isAuth(req, res, next) {
	req.session.reload(function (err) {
		console.log("reload:", req.session);
		console.log("session:", req.session);
		console.log("url path:", req.path);
		if (req.session.login) {
			console.log("auth success");
			return next();
		} else {
			console.log("Need to auth");
			return res.redirect("/");
		}
	});
}


/**
 * @api {get} /status API sanity check
 * @apiName Check
 * @apiDescription	Check if API if up
 * @apiGroup Check
 *
 * @apiSuccess {String} status  Boolean status
 *     HTTP/1.1 200 OK
 *     {
 *         status: "Success"
 *     }
 */
router.get("/status", (req, res) => {
	//console.log("status", req.session);
	res.status(200).send({ status: "Success" });
});

/**
 * @api {post} / login
 * @apiName Login
 * @apiDescription	Login
 * @apiGroup Login
 *
 * @apiParam {String} login
 */
router.post("/", (req, res) => {
	let sess = req.session;

	sess.info.login = req.body.login;
	console.log("headers:", req.headers);
	console.log("/:", sess);
	//req.session.save(function (err) { res.status(200).send({ status: "log" });});
	res.status(200).send({ status: "log" });
});

router.use("/lobbies", lobbiesRoute);

router.use("/users", usersRoute);

export default router;
