
// Check all argument receive from post request
function	addUser(req, res) {
	// Check method type sent
	if (req.method !== "POST")
		return res.status(400).send({error: "Invalid method type received."});
	if (req.body.length < 1)
		return res.status(400).send({error: "Empty body sent"});	
	if (!req.body.login)
		return res.status(400).send({error: "No user login provided"});
	if (req.body.login.length < 5)
		return res.status(400).send({error: "User login must have at least 5 characteres"});

	return true;
}

/*
 *	Check parameter sent before update User
 */
function	updateUser(req, res) {
	// Check method type sent
	if (req.method !== "PUT")
		return res.status(400).send({error: "Invalid method type received."});
	if (req.body.length < 1)
		return res.status(400).send({error: "Empty body sent"});	
	if (!req.body.login)
		return res.status(400).send({error: "No user login provided"});
	return true;
}

export { addUser, updateUser }; 
