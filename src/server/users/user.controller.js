// Check id socket id already exist

import User from "./user.model";
import * as valid from "./user.valid";
import mongoose from "mongoose";

function	get(req, res) {
	res.status(200).send(req.user);
}

function	list(req, res, next) {
	User.list()
		.then(users => res.status(200).send({ users: users }))
		.catch(e => next(e));
}

/*
 *	Load users and append to req
 */
function	load(req, res, next, id) {
	// Check id receives
	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send({ error: "can't find user with id: " + id });

	User.get(id)
		.then(user => {
			req.user = user;
			return next();
		})
		.catch(e => next(e));
}

/*
 *	Create new user
 */
function 	create(req, res, next) {

	//  Check validity of arguments
	if (valid.addUser(req, res) === true) {
console.log(req.header);
		const user = new User({
			login: req.body.login,
			cookie: req.cookies,
			socketId: req.body.socketId
		});
		user.save()
			.then(savedUser => res.status(200).send({ status: true, userId: savedUser._id }))
			.catch(e => next(e));
	}
}

/*
 *	Update concern user
 */
function	update(req, res, next) {
	// Valid param received
	if (valid.updateUser(req, res) === true) {
		const user = req.user;

		if (req.body.login)
			user.login = req.body.login;
		if (req.body.cookie)
			user.cookie = req.body.cookie;
		if (req.body.socketId)
			user.socketId = req.body.socketId;

		user.updateAt = new Date();
		user.save()
			.then(updateUser => res.status(200).send({ status: true, user: updateUser }))
			.catch(e => next(e));
	}
}

export { load, get, list, create, update }; 
