// Importing node modules
import express from "express";
import session from "express-session";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import moment from "moment";
import http from "http";
import SocketIO from "socket.io";

// Importing source files
import * as config from "../../params";
import routes from "./index.routes";
import * as fctSocket from "./socket";

const params = config.dev.server; 
const mongoConfig = config.dev.mongoDb;
const serverConfig = config.dev.server;

// Mongo connection
const url = "mongodb://" + mongoConfig.username + ":" + mongoConfig.password + "@" + serverConfig.host + ":27017/" + mongoConfig.database;
mongoose.connect(url);
mongoose.connection.on("error", console.error.bind(console, "connection error:"));

// Define parameters for api
const app = express();

function	checkLogin(req, res, next) {
	console.log("session:", req.session);
	console.log("url path:", req.path);
	if (!req.session.info) 
		req.session.info = {};
	if (req.session.info.nb)
		req.session.info.nb++;
	else
		req.session.info.nb = 1;
	next();
}

let date = moment();

app.use(cookieParser());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
	secret: "fdp",
	resave: false,
	saveUninitialized: true
}));

app.use(checkLogin);

// Define header for all request
app.use((req, res, next) => {
	res.set("Access-Control-Allow-Origin", ["*"]);
	res.set("Access-Control-Allow-Credentials", true);
	res.set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
	res.set("Access-Control-Allow-Headers", "Content-Type");
	res.set("Content-Type", "application/json");

	console.log(moment().format("DD/MM/YYYY HH:MM:ss"), req.method, "=>", req.originalUrl);
	next();
});

const HTTPserver = http.Server(app);
let io = new SocketIO(HTTPserver);
io.on("connection", (socket) => {
	console.log("socket id:", socket.id);
	socket.on("connectionUser", fctSocket.connection);
});

// Create server
const server = HTTPserver.listen(params.port, () => {
 	// destructuring
	const {address, port} = server.address();

	// string interpolation:
	console.log(`Redtetris app listening at http://${address}:${port}`);
});

// Prefix all routes
app.use("/api", routes);

export default server;
