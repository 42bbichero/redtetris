import Lobby from "./lobby.model";
import * as valid from "./lobby.valid";
import mongoose from "mongoose";

function	get(req, res) {
	res.status(200).send(req.lobby);
}

function	list(req, res, next) {
	Lobby.list()
		.then(lobby => res.status(200).send({ lobbies: lobby }))
		.catch(e => next(e));
}

/*
 *	Load lobby and append to req
 */
function	load(req, res, next, id) {
	// Check id received
	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send({ error: "can't find lobby with id: " + id });

	Lobby.get(id)
		.then(lobby => {
			req.lobby = lobby;
			return next();
		})
		.catch(e => next(e));
}

/*
 *	Create new lobby
 */
function 	create(req, res, next) {
	//  Check validity of arguments
	if (valid.addLobby(req, res) === true) {
		const lobby = new Lobby({
			name: req.body.name,
			users: req.body.users
		});
		lobby.save()
			.then(savedLobby => res.status(200).send({ status: true, lobbyId: savedLobby._id }))
			.catch(e => next(e));
	}
}

/*
 *	Update concern lobby
 */
function	update(req, res, next) {
	// Valid param received
	if (valid.updateLobby(req, res) === true) {
		const lobby = req.lobby;
		if (req.body.status)
			lobby.status = req.body.status;
		if (req.body.usersId) {
			if (!(lobby.usersId = valid.areValidUsersId(req.body.usersId)))
				return res.status(400).send({error: "Invalid usersId provided."});
		}
		lobby.updatedAt = new Date() ;
		lobby.save()
			.then(updateLobby => res.status(200).send({ status: true, lobby: updateLobby }))
			.catch(e => next(e));
	}
}

export { load, get, list, create, update }; 
