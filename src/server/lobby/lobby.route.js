// Import node module
import express from "express";
import * as LobbyCtrl from "./lobby.controller";
const router = express.Router();

/**
 * @api {get} /lobbies/		All lobbies list
 * @apiName GetLobbies
 * @apiDescription		List all lobbies created
 * @apiGroup Lobbies
 *
 * @apiSuccess {Object} lobbies	Created lobbies
 */
router.get("/", LobbyCtrl.list);

/**
 * @api {post} /lobbies/add	Add new lobby
 * @apiName AddLobby
 * @apiDescription		Add a new lobby in DB
 * @apiGroup Lobbies
 *
 * @apiParam {String} name	Name of Lobby
 * @apiParam {String[]} usersId	Users Id of each user present in lobby
 *
 * @apiSuccess {Boolean} status Boolean status of the document creation
 * @apiSuccess {String}	lobbyId	Id of new lobby created
 */
router.post("/add", LobbyCtrl.create);

/**
 * @api {get} /lobbies/:lobbyId	Lobby information
 * @apiName GetLobby
 * @apiDescription		Get informations of a particular lobby
 * @apiGroup Lobbies
 *
 * @apiParam {Number}	lobbyId	MongoDB Id of a particular lobby
 *
 * @apiSuccess {Object} lobbies	Created lobbies
 */
router.route("/:lobbyId")
	.get(LobbyCtrl.get)
/**
 * @api {put} /lobbies/:lobbyId	Edit lobby informations
 * @apiName EditLobby
 * @apiDescription		Edit informations of a particular lobby
 * @apiGroup Lobbies
 *
 * @apiParam {String[]}	usersId	usersId of a particular users
 * @apiParam {String}	status	Status of lobby
 *
 * @apiSuccess {Object} lobbie	New information of updated lobby
 */
	.put(LobbyCtrl.update);

// When lobbyId is sent in param, call load function and append lobby to request 
router.param("lobbyId", LobbyCtrl.load);

// Exporting an object as the default import for this module
export default router;
