import _ from "lodash";
import mongoose from "mongoose";
import Lobby from "./lobby.model";

/*
 *	Check usersId format
 */
function	areValidUsersId(usersId, res) {
	let parsedIds = null;

	// parse array received
	try {
		parsedIds = JSON.parse(usersId);
	} catch (e) {
		return false;
	}
	for (let i = 0; i < parsedIds.length; i++) {
		if (!mongoose.Types.ObjectId.isValid(parsedIds[i]))
			return false;
	}
	return parsedIds;
}

/*
 *	Check all argument receive from post request
 */
function	addLobby(req, res) {
	// Check method type sent
	if (req.method !== "POST")
		return res.status(400).send({error: "Invalid method type received."});
	if (req.body.length < 1)
		return res.status(400).send({error: "Empty body sent"});	
	if (!req.body.name)
		return res.status(400).send({error: "No lobby name provided"});
	if (req.body.name.length < 5)
		return res.status(400).send({error: "Lobby name must have at least 5 characteres"});

	return true;
}

/*
 *	Check parameter sent before update Lobby
 */
function	updateLobby(req, res) {
	let message = null;
	let lobby = new Lobby();
	let statusEnums = lobby.schema.path("status").enumValues;

	if (req.method !== "PUT")
		return res.status(400).send({error: "Invalid method type received."});
	if (req.body.length < 1)
		return res.status(400).send({error: "Empty body sent"});	
	if (req.body.name)
		return res.status(400).send({error: "Can't update lobby name."});
	if (req.body.status && statusEnums.indexOf(req.body.status) < 0)
		return res.status(400).send({error: "Invalid status received."});
	return true;
}

export { addLobby, updateLobby, areValidUsersId }; 
