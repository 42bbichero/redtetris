// React dependencies
import React from 'react';

// Style dependencies
import './board.scss';

// Component dependencies
import {
  Grid
} from '../grid/grid';

/**
 * Component that create a game board
 */
export const Board = () => {
  // Return the render of the component
  return (
    <div className="board-container">
      <Grid/>
    </div>
  );
};
