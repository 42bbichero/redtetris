// React dependencies
import React from 'react';

// Style dependencies
import './bloc.scss';

/**
 * Component that generate a tetris bloc
 */
export const Bloc = (props) => {
  // Define the pool color for blocs
  const color = [
    'disable-bloc',
    'blue-bloc',
    'pink-bloc',
    'green-bloc',
    'orange-bloc',
    'yellow-bloc',
    'red-bloc',
    'purple-bloc',
    'dead-bloc',
  ];

  // Return a stylized tetris bloc
  return (
    <div className={`base-bloc ${color[props.colorIndex]}`}>
      <div className="shine-effect"></div>
    </div>
  );
};
