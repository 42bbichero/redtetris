// React dependencies
import React from 'react';

// Style dependencies
import './header.scss';

/**
 * Component that create an app header
 */
export const Header = () => {
  // Define the game name
  const gameName = 'RED TETRIS';

  // Return the render of the component
  return (
    <span className="head-container">
      <div className="neon-border glow-blue">
        <div className="glow-pink">
          {gameName}
        </div>
      </div>
    </span>
  );
};
