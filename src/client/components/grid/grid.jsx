// React dependencies
import React from 'react';

// Style dependencies
import './grid.scss';

// Component dependencies
import {
  Bloc
} from '../bloc/bloc';

/**
 * Gnerate a line for the grid
 */
const Line = (columnIndex) => {
  // Set the column size
  const column = 10;

  // Holds the line configuration
  let line = [];

  // Fill the line
  for (var x = 0; x < column; x++) {
    line.push(<Bloc key={columnIndex + x} colorIndex={getRandomInt(9)}  />);
  }

  // Return a line
  return (
    <div key={columnIndex} className='line'>
      { line }
    </div>
  );
};

/**
 * Generate a complete grid for the game
 */
export const Grid = () => {
  // Set the line size
  const line = 20;

  // Holds the columns configuration
  let column = [];

  // Fill the columns
  for (var y = 0; y < line; y++) {
    column.push(Line(y));
  }

  // Return all the column
  return (
    <div className='column'>
      { column }
    </div>
  );
};

// // // // // // // // // // // // // // // // // // //
//                                                    //
//            TODO: Testing need to remove            //
//                                                    //
// // // // // // // // // // // // // // // // // // //
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
