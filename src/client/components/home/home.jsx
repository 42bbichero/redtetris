// React dependencies
import React from 'react';
// import * as fct from '../../../library/utils';
// import * as config from '../../../../params';

// const params = config.dev.server;
// const mongoConfig = config.dev.mongoDb;
// const serverConfig = config.dev.server;

// Style dependencies
import './home.scss';

import 'segoe-fonts';

// import { Link } from 'react-router-dom';

/**
 * Stores the login
 */
let login = false;

/**
 * Stores the lobby
 */
let lobby = false;

let $startButton = 'Join the game';

/**
 * Function that keep the input text up to date
 */
function getLogin( event ) {
  login = event.target.value;
}

/**
 * Function that keep the input text up to date
 */
function getLobby( event ) {
  lobby = event.target.value;
}

/**
 * Function that handle the query action on click
 */
function setLogin() {
  console.log( 'login = ', login );
  console.log( 'lobby = ', lobby );
  // fct.POST(serverConfig.getUrl() + '/api/', { login: login }, (err, result) => {
  //   if (err)
  //     console.log('err:', err);
  //   console.log('result:', result);
  // });
}

/**
 * Component that create the party zone
 */
export const Home = () => {
  // Return the render of the component
  return (
    <div className="home">

      {/* Holds the player infos */}
      <div className="player-infos">

        {/* Login form */}
        <form className="form glow-purple">
          <input className="input" type="text" name="value" placeholder="Username" onChange={evt => getLogin( evt )}/>
        </form>

        {/* Lobby form */}
        <form className="form glow-purple">
          <input className="input" type="text" name="value" placeholder="Lobby" onChange={evt => getLobby( evt )}/>
        </form>

        {/* Action button */}

        {/* <Link to="/party" style={{ textDecoration: 'none' }}> */}

        <div className="form glow-red button" onClick={setLogin}> {$startButton} </div>

        {/* </Link> */ }

      </div>

    </div>
  );
};
