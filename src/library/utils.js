import request from "request";

const POST = (url, form, callback) => {
	console.log("url:", url);
	console.log("form:", form);
	request.post({
		url: url,
		form: form,
		headers: {
			"Access-Control-Allow-Headers": "Accept",
			"Access-Control-Allow-Credentials": "true",
			"Access-Control-Request-Method": "POST",
			"Content-Type": "application/json",
		}, function (err, body) {
			if (err)
				return callback(err);
			let data;
			try {
				data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			return callback(data);
		}
	});
};

const GET = (url, form, callback) => {
	request({
		url: url,
		form: form,
		headers: {
			"Content-Type": "application/json"
		}, function (err, body) {
			if (err)
				return callback(err);
			let data;
			try {
				data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			return callback(data);
		}
	});
};

export { POST, GET };
