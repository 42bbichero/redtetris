/* eslint-disable */
var path = require('path');

module.exports = {
		node: {
			  fs: 'empty',
				  tls: "empty",
			net: "empty"
		},
  //  Set the mode
  mode: 'development',

  entry: './src/client/index.jsx',

  module: {
    rules: [
      // Babel Jsx Loader
      {
        test: /\.jsx?/,
        exclude: /(node_modules|bower_components)/,

        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env', 'react'],
          },
        },

        resolve: {
          extensions: ['.js', '.jsx', '.json']
        }

      },
      // Styles Loaders
      {
        test: /\.(scss|css)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
        include: [
          path.resolve(__dirname, './src'),
          path.resolve(__dirname, 'node_modules')
        ],
      },
      // Images Files Loaders
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader', {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
            },
          }
        ]
      },
      // Multi Files Loader
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      }
      // TODO: New Rule Here
    ],
  },

  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/',
  },

  // Make the client catchable locally
  devServer: {
    disableHostCheck: true,
    historyApiFallback: true,
  }

};
